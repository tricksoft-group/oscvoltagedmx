/**
 * @author Nassim Ourami
 */

const osc = new OSC();
osc.open();

let instantVoltage = getFromStorage('instantVoltage') ?? {};
let voltage = getFromStorage('voltage') ?? {};
let dmxValues = getFromStorage('dmxValues') ?? {};
let voltageLimit = getFromStorage('voltageLimit') ?? 1000;

let globalVoltage = 10;
let channelLimit = 512;
let volageClasses = {
	50: 'bg-success',
	65: 'bg-info',
	75: 'bg-warning',
	95: 'bg-danger',
};

let $bar = document.getElementById('voltage-bar');
let $voltageInput = document.getElementById('voltage-limit');
$voltageInput.value = voltageLimit;

let $template = document.getElementById('channel-template');
let $channels = document.getElementById('channels');

function getFromStorage(key) {
	let element = localStorage.getItem(key);
	if (element === null) {
		return element;
	}
	return JSON.parse(element);
}

function initVoltages() {
	let $input;
	for (var i in voltage) {
		$input = document.getElementById('channel-' + i);
		$input.value = voltage[i];
	}
}

function updateLocalStorage() {
	localStorage.setItem('instantVoltage', JSON.stringify(instantVoltage));
	localStorage.setItem('voltage', JSON.stringify(voltage));
	localStorage.setItem('dmxValues', JSON.stringify(dmxValues));
	localStorage.setItem('voltageLimit', JSON.stringify(voltageLimit));
}

/**
 *
 * @param {string} address
 * @returns
 */
function getChannel(address) {
	let elements = address.split('/');
	let channel = parseInt(elements[elements.length - 1]);
	return channel + 1;
}

function assignValueToCircuit(channel, value) {
	instantVoltage[channel] = (voltage[channel] ?? 1) * value;
	saveDmxValue(channel, value);
}

function saveDmxValue(channel, value) {
	dmxValues[channel] = value;
}

/**
 * Assign voltage
 * @param {number} value
 * @param {number} channel
 */
function assignVoltageToChannel(value, channel) {
	voltage[channel] = value;
}

/**
 * Return css class
 * @param {number} percent
 * @returns
 */
function getCssClasses(percent) {
	if (percent <= 50) {
		return volageClasses[50];
	}
	if (percent < 75) {
		return volageClasses[65];
	}
	if (percent < 95) {
		return volageClasses[75];
	}
	if (percent >= 95) {
		return volageClasses[95];
	}
	return volageClasses[50];
}

function updateBar(newVoltage) {
	let percent = (newVoltage / voltageLimit) * 100;
	let cssClass = getCssClasses(percent);

	$bar.ariaValueNow = percent;
	$bar.style.width = percent + '%';
	$bar.classList.remove(volageClasses[50], volageClasses[65], volageClasses[75], volageClasses[95]);
	$bar.classList.add(cssClass);
	$bar.innerHTML = Math.round(newVoltage) + ' w (' + Math.round(percent) + '%)';
}

function updateGlobalVoltage() {
	let newVoltage = 0;
	for (let i in instantVoltage) {
		newVoltage += instantVoltage[i];
	}
	globalVoltage = newVoltage;
	updateBar(globalVoltage);
}

function generatePatchGridForChannel(id) {
	let $clone = document.importNode($template.content, true);
	let $input = $clone.querySelectorAll('.channel');
	let $label = $clone.querySelectorAll('.channel-label');

	$label[0].attributes['for'] = 'channel-' + id;
	$label[0].innerHTML = 'Channel ' + id + ' en W';
	$label[0].innerText = 'Channel ' + id + ' en W';
	$input[0].id = 'channel-' + id;

	$channels.appendChild($clone);
}

$voltageInput.addEventListener('input', (e) => {
	voltageLimit = e.target.value > 0 ? e.target.value : 1000;
	updateBar(globalVoltage);
	updateLocalStorage();
});

osc.on('/0/dmx/*', (message) => {
	assignValueToCircuit(getChannel(message.address), message.args[0]);
	updateGlobalVoltage();
	updateLocalStorage();
});

new Promise((resolve) => {
	let limit = channelLimit + 1;
	for (let i = 1; i < limit; i++) {
		generatePatchGridForChannel(i);
	}
	resolve();
})
	.then(() => {
		let $channelsInput = document.querySelectorAll('.channel');
		let elementsArray = Array.prototype.slice.call($channelsInput);
		initVoltages();
		elementsArray.forEach((elem) => {
			let idd = elem.id.split('-');
			let channeld = parseInt(idd[1]);

			assignVoltageToChannel(parseInt(elem.value), channeld);

			// Assign an event handler
			elem.addEventListener('input', (e) => {
				let id = e.target.id.split('-');
				let channel = parseInt(id[1]);

				assignVoltageToChannel(parseInt(e.target.value), channel);
				assignValueToCircuit(channel, dmxValues[channel] ?? 0);
				updateGlobalVoltage();
				updateLocalStorage();
			});
		});
	})
	.then(() => {
		updateLocalStorage();
	});

function downloadJson(json, filename) {
	var jsonFile;
	var downloadLink;

	if (
		window.Blob == undefined ||
		window.URL == undefined ||
		window.URL.createObjectURL == undefined
	) {
		alert("Your browser doesn't support Blobs");
		return;
	}

	jsonFile = new Blob([json], { type: 'text/json' });
	downloadLink = document.createElement('a');
	downloadLink.download = filename;
	downloadLink.href = window.URL.createObjectURL(jsonFile);
	downloadLink.style.display = 'none';
	document.body.appendChild(downloadLink);
	downloadLink.click();
}

let $saveButton = document.getElementById('save-button');
$saveButton.addEventListener('click', (e) => {
	let jsonData = {
		voltage: voltage,
		voltageLimit: voltageLimit,
	};
	downloadJson(JSON.stringify(jsonData), 'Sauvegarde OCS DMX.json');
});

let $form = document.getElementById('load-config');
$form.addEventListener('submit', (e) => {
	e.preventDefault();
	let file = document.getElementById('config');
	if (file.files.length) {
		let reader = new FileReader();

		reader.onload = function (e) {
			let config = JSON.parse(e.target.result);

			if (typeof config.voltage != 'undefined') {
				voltage = config.voltage;
				initVoltages();
			}
			if (typeof config.voltage != 'undefined') {
				voltageLimit = config.voltageLimit;
				$voltageInput.value = config.voltageLimit;
			}
		};

		//reader.readAsBinaryString(file.files[0])
		reader.readAsText(file.files[0]);
		let myToastEl = document.getElementById('alert');
		let myToast = bootstrap.Toast.getOrCreateInstance(myToastEl);
		myToast.show();
	}
});
