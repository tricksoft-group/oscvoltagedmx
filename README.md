# oscVoltageDmx

Voltage meter from OSC

Affichage d'une page de contrôle de la consommation électrique à partir de messages OCS. Les messages OCS

<img alt="Logo" align="right" src="/preview.png" />

## Getting started

Boitier ENTTEC -> https://qlcplus.org/downloads.html -> OSC

Qlc+ est utilisé pour la conversion DMX->OSC mais il est tout à fait utiliser Dlight.

L'adresse d'écoute est la suivante :

```
\dmx\univers\channel
```

Téléchargez et installez qlcplus, nodejs & npm puis sur linux

```
./start.sh
```

## License

Open source
