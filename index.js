const OSC = require('osc-js');
const open = require('open');

open('./index.html', { wait: true });

const config = { udpServer: { host: '127.0.0.1', port: 9000 } };
const osc = new OSC({ plugin: new OSC.BridgePlugin(config) });
osc.open(); // start a WebSocket server on port 8080
